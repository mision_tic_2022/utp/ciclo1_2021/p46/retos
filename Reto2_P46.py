
def recomendaciones_zapatos(diccionario: dict)-> dict:
    
    #Resultado para los condicionales
    precio = diccionario['precio']
    c1 = diccionario['botas']
    c2 = diccionario['cuero']
    c3 = diccionario['antideslizante']
    c4 = diccionario['importado']
   

    if precio > 200000:
        if c1:
            if c2:
                if c3:
                    if c4:
                        recomendacion = "Caterpilla"
                    else:
                        recomendacion = "Bata"
                else:
                    recomendacion = "Zara"
            else:
                recomendacion = "Adidas"
        else:
            if c4:
                recomendacion = "Nike"
            else:
                recomendacion = "Crocs"
    else:
        if c1:
            if c2:
                if c3:
                    if c4:
                        recomendacion = "Panama jack"
                    else:
                        recomendacion = "Arturo calle"
                else:
                    recomendacion = "Croydon"
            else:
                recomendacion = "Bubble gummers"
        else:
            recomendacion = "Evacol"
    
    diccionario_respuesta = {
        "cliente": str(diccionario['nombre'])+" "+str(diccionario['apellido']),
        "recomendación": recomendacion
    }
    return diccionario_respuesta

