''' 
FunciĂłn para calcular el promedio semanal de vacunados contra el covid-19 
'''
def promedio_vacunados(region: str, vacunados_lunes: int, vacunados_martes: int, vacunados_miercoles: int, vacunados_jueves: int, vacunados_viernes: int, vacunados_sabado: int, vacunados_domingo: int)->str:
    promedioSemanal= (vacunados_lunes + vacunados_martes + vacunados_miercoles + vacunados_jueves + vacunados_viernes + vacunados_sabado + vacunados_domingo) / 7 # Calcular Promedio
    promedioRedondeado= round(promedioSemanal,1) # Promedio redondeado a 1 decima
    promedio= str(promedioRedondeado) # Salida convertida a cadena 

    return "El promedio de vacunados semanal contra el covid-19 en {} es de {}.".format(region, promedio)


print(promedio_vacunados("Bogotá", 250, 700, 400, 500, 230, 430, 700))
print(promedio_vacunados("Cucuta", 300, 240, 258, 210, 450, 530, 220))
print(promedio_vacunados("Bucaramanga", 580, 900, 950, 640, 760, 540, 1200))