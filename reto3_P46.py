

def calcular_ingresos(datos: list) -> dict:
    # Tipos de planes
    prepago: str = "prepago"
    postpago: str = "postpago"
    # Variable para almacenar el total de todos los planes
    total: int = 0
    # Variable/contador de los planes prepagos
    cant_prepago: int = 0
    # Variable/contador de los planes postpagos
    cant_postpago: int = 0
    # Variable que representa los ingresos totales de plan prepago
    total_prepago: int = 0
    # Variable que representa los ingresos totales de plan postpago
    total_postpago: int = 0
    # Itera los datos a procesar
    for item in datos:
        # Suma el total de ingresos
        total += item["valor_pagar"]
        # Obtiene el total de los planes prepagos y postpagos
        if item["tipo_plan"] == prepago:
            cant_prepago += 1
            total_prepago += item["valor_pagar"]
        elif item["tipo_plan"] == postpago:
            cant_postpago += 1
            total_postpago += item["valor_pagar"]

    promedio_prepago: float = 0
    promedio_postpago: float = 0
    # Valida para que no se dé un caso de 0/0
    if cant_prepago > 0:
        promedio_prepago = round((total_prepago / cant_prepago), 1)
    if cant_postpago > 0:
        promedio_postpago = round((total_postpago / cant_postpago), 1)
    # Debe retornar
    resp: dict = {
        "total": total,
        "promedio_prepago": promedio_prepago,
        "promedio_postpago": promedio_postpago
    }

    return resp


# caso 1
datos: list = [
    {
        "tipo_plan": "prepago",
        "valor_pagar": 100000
    },
    {
        "tipo_plan": "postpago",
        "valor_pagar": 50000
    },
    {
        "tipo_plan": "prepago",
        "valor_pagar": 250000
    },
    {
        "tipo_plan": "postpago",
        "valor_pagar": 480000
    },
    {
        "tipo_plan": "prepago",
        "valor_pagar": 20000
    },
    {
        "tipo_plan": "prepago",
        "valor_pagar": 40000
    },
    {
        "tipo_plan": "postpago",
        "valor_pagar": 150000
    }
]
print(calcular_ingresos(datos))

# caso 2
datos: list = [
    {
        "tipo_plan": "prepago",
        "valor_pagar": 300000
    },
    {
        "tipo_plan": "prepago",
        "valor_pagar": 460000
    },
    {
        "tipo_plan": "prepago",
        "valor_pagar": 105000
    },
    {
        "tipo_plan": "prepago",
        "valor_pagar": 90000
    },
    {
        "tipo_plan": "prepago",
        "valor_pagar": 50000
    },
    {
        "tipo_plan": "prepago",
        "valor_pagar": 95000
    },
    {
        "tipo_plan": "prepago",
        "valor_pagar": 68000
    }
]
print(calcular_ingresos(datos))

#Caso 3
datos: list = [
    {
        "tipo_plan": "postpago",
        "valor_pagar": 20000
    },
    {
        "tipo_plan": "postpago",
        "valor_pagar": 85000
    },
    {
        "tipo_plan": "postpago",
        "valor_pagar": 46000
    },
    {
        "tipo_plan": "postpago",
        "valor_pagar": 58000
    },
    {
        "tipo_plan": "postpago",
        "valor_pagar": 305000
    },
    {
        "tipo_plan": "postpago",
        "valor_pagar": 78000
    },
    {
        "tipo_plan": "postpago",
        "valor_pagar": 199000
    }
]
print(calcular_ingresos(datos))

#Caso 4
datos: list = [
    {
        "tipo_plan": "prepago",
        "valor_pagar": 30000
    },
    {
        "tipo_plan": "postpago",
        "valor_pagar": 25000
    },
    {
        "tipo_plan": "prepago",
        "valor_pagar": 10000
    },
    {
        "tipo_plan": "postpago",
        "valor_pagar": 12000
    },
    {
        "tipo_plan": "prepago",
        "valor_pagar": 8000
    },
    {
        "tipo_plan": "postpago",
        "valor_pagar": 5000
    },
    {
        "tipo_plan": "prepago",
        "valor_pagar": 2000
    }
]
print(calcular_ingresos(datos))

